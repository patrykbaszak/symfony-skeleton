<?php

declare(strict_types=1);

namespace App\DTO\User;

/**
 * @method string|null getPassword()
 */
class Secrets
{
    /**
     * @param string $password password hash
     * @param string $secret   secret hash
     */
    public function __construct(
        /* @var string $password password hash */
        private ?string $password = null,
        /* @var string $secret Secret hash */
        private ?string $secret = null,
    ) {
    }

    /**
     * @param string $key secret id
     *
     * @return string|null secret value or hash
     */
    public function get(string $key): ?string
    {
        return $this->{$key} ?? null;
    }

    /**
     * @param string      $key   secret id
     * @param string|null $value secret value, set null if You want erase value
     */
    public function set(string $key, ?string $value): void
    {
        $this->{$key} = $value;
    }
}
