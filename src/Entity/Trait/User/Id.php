<?php

declare(strict_types=1);

namespace App\Entity\Trait\User;

use Ramsey\Uuid\Uuid;

trait Id
{
    public function getId(): ?string
    {
        return $this->uuid ?? null;
    }

    private function setUuid(): self
    {
        if ($this->getId()) {
            return $this;
        }
        $this->uuid = Uuid::uuid4();

        return $this;
    }
}
