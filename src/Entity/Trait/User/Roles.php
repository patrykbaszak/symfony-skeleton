<?php

declare(strict_types=1);

namespace App\Entity\Trait\User;

use App\Entity\User;

/**
 * @method User updateMofified()
 */
trait Roles
{
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = array_unique($roles);
        $this->updateModified();

        return $this;
    }
}
