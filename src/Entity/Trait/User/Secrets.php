<?php

declare(strict_types=1);

namespace App\Entity\Trait\User;

use App\Entity\User;

/**
 * @method User updateMofified()
 */
trait Secrets
{
    public function getSpecificSecret(string $key): ?string
    {
        return $this->secrets->get($key);
    }

    public function setSpecificSecret(string $key, ?string $value): self
    {
        $this->secrets->set($key, $value);
        $this->updateMofified();

        return $this;
    }

    public function getSecret(): ?string
    {
        return $this->secrets->get('secret');
    }

    public function setSecret(?string $secretHash): self
    {
        $this->secrets->set('secret', $secretHash);
        $this->updateMofified();

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->secrets->get('password');
    }

    public function setPassword(?string $passwordHash): self
    {
        $this->secrets->set('password', $passwordHash);
        $this->updateMofified();

        return $this;
    }

    public function getSalt(): ?string
    {
        global $_ENV;

        return $_ENV['APP_SECRET'] ?? null;
    }
}
