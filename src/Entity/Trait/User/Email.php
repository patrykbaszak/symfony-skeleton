<?php

declare(strict_types=1);

namespace App\Entity\Trait\User;

use App\Entity\Email as EntityEmail;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;

/**
 * @method User updateMofified()
 */
trait Email
{
    /**
     * @return Collection|EntityEmail[]
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(EntityEmail $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails[] = $email;
            $email->setUser($this);
        }
        $this->updateMofified();

        return $this;
    }

    public function removeEmail(EntityEmail $email): self
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getUser() === $this) {
                $email->setUser(null);
            }
        }
        $this->updateMofified();

        return $this;
    }
}
