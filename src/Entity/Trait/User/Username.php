<?php

declare(strict_types=1);

namespace App\Entity\Trait\User;

use App\Entity\User;

/**
 * @method User updateMofified()
 */
trait Username
{
    public function getUserIdentifier(): string
    {
        return $this->username ?? $this->uuid;
    }

    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;
        $this->updateMofified();

        return $this;
    }
}
