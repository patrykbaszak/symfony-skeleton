<?php

namespace App\Entity;

use App\DTO\User\Secrets;
use App\Entity\Trait\User\Email;
use App\Entity\Trait\User\Id;
use App\Entity\Trait\User\Roles;
use App\Entity\Trait\User\Secrets as UserSecrets;
use App\Entity\Trait\User\Username;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use Id;
    use Username;
    use Email;
    use Roles;
    use UserSecrets;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=36, unique=true)
     */
    private string $uuid;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private ?string $username;

    /**
     * @ORM\OneToMany(targetEntity=Email::class, mappedBy="User")
     */
    private ArrayCollection $emails;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="object")
     */
    private Secrets $secrets;

    /**
     * @var int timestamp
     * @ORM\Column(type="integer")
     */
    private int $created;

    /**
     * @var int|null timestamp
     * @ORM\Column(type="integer")
     */
    private ?int $modified;

    public function __construct()
    {
        $this->emails = new ArrayCollection();
    }

    public static function createNew(
        ?string $username = null,
        ?string $email = null,
        ?string $passwordHash = null,
        ?string $secretHash = null,
        array $roles = [],
    ): self {
        return (new User())
            ->setUuid()
            ->setUsername($username)
            ->addEmail($email)
            ->setPassword($passwordHash)
            ->setSecret($secretHash)
            ->setRoles($roles)
            ->setCreated()
            ->clearModified();
    }

    private function setCreated(): self
    {
        if ($this->created) {
            return $this;
        }
        $this->created = time();

        return $this;
    }

    private function updateModified(): self
    {
        $this->modified = time();

        return $this;
    }

    private function clearModified(): self
    {
        $this->modified = null;

        return $this;
    }

    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
