<?php

declare(strict_types=1);

namespace App\Server\Query;

use Swoole\Http\Request;

class GetHttpRequest
{
    /**
     * @param Request $request Swoole Request object
     * @phpstan-ignore-next-line
     */
    public function __construct(private Request $request)
    {
    }

    /**
     * @return Request Swoole Request object
     * @phpstan-ignore-next-line
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}
