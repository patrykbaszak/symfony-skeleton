<?php

declare(strict_types=1);

namespace App\Server;

use App\Kernel;
use App\Server\Command\TerminateSwooleResponse;
use App\Server\Query\GetHttpRequest;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server as WebSocket;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

class Swoole
{
    use HandleTrait;

    /**
     * @var WebSocket Swoole Server object
     * @phpstan-ignore-next-line
     */
    private WebSocket $server;

    public const WEB_SOCKET = 'ws';
    public const HTTP = 'http';

    /**
     * @param Kernel $kernel
     */
    public function __construct(private Kernel $kernel, MessageBusInterface $messageBus)
    {
        /* @phpstan-ignore-next-line */
        $this->server = new WebSocket('0.0.0.0', 9502);
        $this->messageBus = $messageBus;
    }

    /**
     * @param string ...$options
     */
    public function register(string ...$options): void
    {
        $this->setServerConfig();
        $this->setOnServerStartCallback();
        if (in_array(self::HTTP, $options) || empty($options)) {
            $this->setOnHttpRequestCallback();
        }
        if (in_array(self::WEB_SOCKET, $options)) {
            $this->setOnOpenWebSocketConnectionCallback();
            $this->setOnWebSocketRequestCallback();
            $this->setOnWebSocketMessageCallback();
            $this->setOnCloseWebSocketConnectionCallback();
        }
    }

    public function start(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->start();
    }

    private function setServerConfig(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->set([]);
    }

    private function setOnServerStartCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Start', function (WebSocket $server) {
        });
    }

    private function setOnHttpRequestCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Request', function (Request $request, Response $response) {
            try {
                $this->handle(
                    new TerminateSwooleResponse(
                        $response,
                        $this->kernel->handle($this->handle(
                            new GetHttpRequest($request)
                        ))
                    )
                );
            } catch (Throwable $e) {
                /* @phpstan-ignore-next-line */
                $response->status($e->getCode());
                /* @phpstan-ignore-next-line */
                $response->end(
                    'message: '.$e->getMessage().PHP_EOL.
                    'stack trace: '.$e->getTraceAsString().PHP_EOL
                );
            }
        });
    }

    private function setOnOpenWebSocketConnectionCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Open', function (WebSocket $server, Request $request) {
        });
    }

    private function setOnWebSocketRequestCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Handshake', function (Request $request, Response $response) {
        });
    }

    private function setOnWebSocketMessageCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Message', function (WebSocket $server, Frame $frame) {
        });
    }

    private function setOnCloseWebSocketConnectionCallback(): void
    {
        /* @phpstan-ignore-next-line */
        $this->server->on('Close', function (WebSocket $server, int $fd) {
        });
    }
}
