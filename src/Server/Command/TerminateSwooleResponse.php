<?php

declare(strict_types=1);

namespace App\Server\Command;

use Swoole\Http\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class TerminateSwooleResponse
{
    /**
     * @param Response     $response     Swoole Response object
     * @param HttpResponse $httpResponse Symfony Response object
     * @phpstan-ignore-next-line
     */
    public function __construct(private Response $response, private HttpResponse $httpResponse)
    {
    }

    /**
     * @return Response Swoole Response object
     * @phpstan-ignore-next-line
     */
    public function getSwooleResponse(): Response
    {
        return $this->response;
    }

    /**
     * @return HttpResponse Symfony Response object
     */
    public function getSymfonyResponse(): HttpResponse
    {
        return $this->httpResponse;
    }
}
