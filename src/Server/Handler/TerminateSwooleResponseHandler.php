<?php

declare(strict_types=1);

namespace App\Server\Handler;

use App\Server\Command\TerminateSwooleResponse;
use Swoole\Http\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class TerminateSwooleResponseHandler implements MessageHandlerInterface
{
    /**
     * @var Response Swoole Response object
     * @phpstan-ignore-next-line
     */
    private Response $swoole;

    /**
     * @var HttpResponse Symfony Response object
     */
    private HttpResponse $symfony;

    public function __invoke(TerminateSwooleResponse $command): void
    {
        $this->swoole = $command->getSwooleResponse();
        $this->symfony = $command->getSymfonyResponse();

        $this->writeHeaders();

        /* @phpstan-ignore-next-line */
        $this->swoole->end(
            $this->symfony->getContent()
        );
    }

    private function writeHeaders(): void
    {
        if (headers_sent()) {
            return;
        }

        foreach ($this->symfony->headers->allPreserveCaseWithoutCookies() as $name => $values) {
            foreach ($values as $value) {
                /* @phpstan-ignore-next-line */
                $this->swoole->header($name, $value);
            }
        }

        /* @phpstan-ignore-next-line */
        $this->swoole->status($this->symfony->getStatusCode());

        foreach ($this->symfony->headers->getCookies() as $cookie) {
            /* @phpstan-ignore-next-line */
            $this->swoole->cookie(
                $cookie->getName(),
                $cookie->getValue() ?? '',
                $cookie->getExpiresTime(),
                $cookie->getPath(),
                $cookie->getDomain() ?? '',
                $cookie->isSecure(),
                $cookie->isHttpOnly()
            );
        }
    }
}
