<?php

declare(strict_types=1);

namespace App\Server\Handler;

use App\Server\Query\GetHttpRequest;
use Swoole\Http\Request;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetHttpRequestHandler implements MessageHandlerInterface
{
    /**
     * @var Request Swoole Request object
     * @phpstan-ignore-next-line
     */
    private Request $request;

    /**
     * @return HttpRequest Symfony Request object
     */
    public function __invoke(GetHttpRequest $query): HttpRequest
    {
        $this->request = $query->getRequest();

        return new HttpRequest(
            $this->getQueryFromGlobals(),
            $this->getPostFromGlobals(),
            [],
            $this->getCookiesFromGlobals(),
            $this->getFilesFromGlobals(),
            $this->getServerVariablesFromGlobals(),
            /* @phpstan-ignore-next-line */
            $this->request->rawContent()
        );
    }

    /**
     * @return string[]
     */
    private function getServerVariablesFromGlobals(): array
    {
        global $_SERVER;
        $headers = array_combine(
            array_map(
                function (string $key) {
                    return 'HTTP_'.str_replace('-', '_', $key);
                },
                /* @phpstan-ignore-next-line */
                array_keys($this->request->header)
            ),
            /* @phpstan-ignore-next-line */
            array_values($this->request->header)
        );

        return array_change_key_case(
            array_merge(
                $_SERVER,
                /* @phpstan-ignore-next-line */
                $this->request->server,
                $headers
            ),
            CASE_UPPER
        );
    }

    /**
     * @return array<string|int|float>
     */
    private function getQueryFromGlobals(): array
    {
        /* @phpstan-ignore-next-line */
        return $this->request->get ?? [];
    }

    /**
     * @return mixed[]
     */
    private function getPostFromGlobals(): array
    {
        /* @phpstan-ignore-next-line */
        return $this->request->post ?? [];
    }

    /**
     * @return array[]
     */
    private function getFilesFromGlobals(): array
    {
        /* @phpstan-ignore-next-line */
        return $this->request->files ?? [];
    }

    /**
     * @return string[]
     */
    private function getCookiesFromGlobals(): array
    {
        /* @phpstan-ignore-next-line */
        return $this->request->cookie ?? [];
    }
}
