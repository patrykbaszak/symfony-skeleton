<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use UnexpectedValueException;

class TokenAuthenticator extends AbstractAuthenticator
{
    public const SUPPORTED_TOKEN_TYPE = 'bearer';
    public const HEADER_NAME = 'authorization';

    public function supports(Request $request): ?bool
    {
        if (!$request->headers->has(self::HEADER_NAME)) {
            return false;
        }

        return true;
    }

    public function authenticate(Request $request): PassportInterface
    {
        if (null === $authorizationHeader = $request->headers->get(self::HEADER_NAME)) {
            throw new UnexpectedValueException('Authorization header missing');
        }
        $token = $this->getCredentials($authorizationHeader);

        return new SelfValidatingPassport(
            new UserBadge(
                $token,
                // function (string $token): User {
                //     return $this->getUser($token);
                // }
            )
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(
            [
                'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
            ],
            Response::HTTP_FORBIDDEN
        );
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new JsonResponse(
            [
                'message' => 'Authentication Required',
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }

    private function getCredentials(string $authorizationHeader): string
    {
        if (!$this->isHeaderSupported($authorizationHeader)) {
            throw new UnexpectedValueException('Invalid authorization header: '.$authorizationHeader);
        }

        return explode(' ', $authorizationHeader)[1];
    }

    private function isHeaderSupported(string $authorizationHeader): bool
    {
        $authorizationHeaderParts = explode(' ', $authorizationHeader);

        return 2 === count($authorizationHeaderParts)
            && self::SUPPORTED_TOKEN_TYPE === strtolower($authorizationHeaderParts[0]);
    }
}
