FROM mariadb:latest

ENV MYSQL_ROOT_PASSWORD=admin
ENV MYSQL_DATABASE=application

COPY /docker/database/mysql/init.sql /data/application/init.sql

VOLUME ["/docker/database/mysql/data:/var/lib/mysql/"]

EXPOSE 3306