FROM mongo:latest

ENV MONGO_INITDB_DATABASE=app
ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=admin

VOLUME ["/docker/database/mongoDB/mongo_init.js:/docker-entrypoint-initdb.d/init-mongo.js:ro"]

EXPOSE 27017