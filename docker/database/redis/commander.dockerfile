FROM rediscommander/redis-commander:latest

ENV REDIS_HOSTS=local:redis:6379

EXPOSE 8081