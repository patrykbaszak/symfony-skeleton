FROM phpswoole/swoole:php8.0-alpine

RUN apk add bash

# install standard extenstions
RUN apk add $PHPIZE_DEPS

# install mongodb
RUN apk add autoconf pkgconfig libressl-dev \
    && pecl install mongodb \
    && docker-php-ext-enable mongodb

# install redis
RUN mkdir -p /usr/src/php/ext/redis \
    && curl -fsSL https://github.com/phpredis/phpredis/archive/5.3.4.tar.gz | tar xvz -C /usr/src/php/ext/redis --strip 1 \
    && docker-php-ext-install redis

# install mysql
RUN docker-php-ext-install pdo_mysql

# install composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

CMD ["/usr/local/bin/php", "/app/server.php"]

EXPOSE 9502
