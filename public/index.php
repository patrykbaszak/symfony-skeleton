<?php

use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__) . '/bootstrap.php';

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
