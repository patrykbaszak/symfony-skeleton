<?php

declare(strict_types=1);

use App\Kernel;
use App\Server\Swoole;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

require_once __DIR__ . '/bootstrap.php';

$server = new Swoole(new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']), new MessageBus([
    new HandleMessageMiddleware(new HandlersLocator([
        App\Server\Command\TerminateSwooleResponse::class => [new App\Server\Handler\TerminateSwooleResponseHandler],
        App\Server\Query\GetHttpRequest::class => [new App\Server\Handler\GetHttpRequestHandler]
    ]))
]));

$server->register(Swoole::HTTP, Swoole::WEB_SOCKET);
$server->start();
